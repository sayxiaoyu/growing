package com.lj.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.lj.domain.Advertisement;
import com.lj.service.AdvertisementService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@Controller
@RequestMapping(value = "/admin/advertisement")
public class AdvertisementController {
	
	@Autowired
	private AdvertisementService advertisementService;
	
	
	
	@RequestMapping(value="/getADByCollectorID",method = RequestMethod.GET)
	@ResponseBody
	public JSONObject getADByCollectorID(HttpServletRequest request,HttpServletResponse response,Model model) 
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String code = "";
		String message = "";
		JSONObject json = new JSONObject();
		String collector_id = request.getParameter("collector_id");
		List<Advertisement> ads = advertisementService.getADByCollectorID(collector_id);
		if(ads == null) {
			code = "0";
			message = "该设备没有广告";
		}else {
			code = "1";
			message = "获取成功";
		}
		json.put("code", code);
		json.put("message", message);
		json.put("result",ads);
		return json;
	}
	
}
