package com.lj.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lj.domain.UserInfo;
import com.lj.service.AssessService;
import com.lj.service.UserService;
import com.lj.websocket.WebSocket;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "消息", description = "demo操作 API", position = 100, protocols = "http")
@RestController
@RequestMapping("/demo")
public class DemoController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AssessService assessService;
	
	@Autowired
    private WebSocket webSocket;
	/*
	 * @RequestMapping(value="/hello") public String index() { return "Hello World";
	 * }
	 */
    
    
	
	  @RequestMapping(value="/getUserByOrder", method = RequestMethod.GET) 
	public List<UserInfo> getUserByName(/* @RequestParam(value = "username") String username */) {
		  List<UserInfo> users = new ArrayList<UserInfo>(); 
		 // users = userService.getUsersByName(username); 
	  return users; 
	  }
	 
    
    @ApiOperation(
			value = "regionKPI",
			notes = "完整的消息内容列表",
			produces="application/json, application/xml",
			consumes="application/json, application/xml",
			response = Map.class)
    @RequestMapping(value="/getRegionKPI", method = RequestMethod.GET)
	public Map<String, Object> getRegionKPI(
											  @RequestParam(value = "region_id") String regionid,
											  
											  @RequestParam(value = "sDate") String sdate,@RequestParam(value =
											  "eDate") String edate
											 ) {
    	//List<UserInfo> users = new ArrayList<UserInfo>();
    	Map<String,Object> maps = assessService.getRegionKPI(regionid, sdate, edate);
    	Map<String,Object> results = new HashMap<String, Object>();
    	results.put("message", "获取成功");
    	results.put("code", "1");
    	results.put("result", maps);
        return results;
    }

	@RequestMapping("/websocketdemo")
    @ResponseBody
    public String createOrder(HttpServletRequest request, HttpServletResponse response) {
        //获取cookie中的订单号
        //String order_numberCookie = getorder_numberCookie(request);
        //long order_number;
		/*
		 * if (order_numberCookie != null) { order_number =
		 * Long.parseLong(order_numberCookie); orderDao.createOrder(order_number);
		 * 
		 * // 获取名为"cart"的cookie Cookie cookie = getCookie(request); if (cookie != null)
		 * { // 设置寿命为0秒 cookie.setMaxAge(0); // 设置路径 cookie.setPath("/"); //
		 * 设置cookie的value为null cookie.setValue(null); // 更新cookie
		 * response.addCookie(cookie); } return "success"; }
		 */
        webSocket.sendMessage("hello");
        return "success";
    }

}
