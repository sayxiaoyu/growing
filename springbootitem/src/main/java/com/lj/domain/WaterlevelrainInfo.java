package com.lj.domain;

import java.sql.Timestamp;

public class WaterlevelrainInfo {
		
	private int id;
	private int station_id;//电排站id
	private Timestamp save_time;//保存时间
	private float out_level;//外江水位
	private float local_level;//内湖水位
	private float rain_fall;//雨量
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStation_id() {
		return station_id;
	}
	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}
	public Timestamp getSave_time() {
		return save_time;
	}
	public void setSave_time(Timestamp save_time) {
		this.save_time = save_time;
	}
	public float getOut_level() {
		return out_level;
	}
	public void setOut_level(float out_level) {
		this.out_level = out_level;
	}
	public float getLocal_level() {
		return local_level;
	}
	public void setLocal_level(float local_level) {
		this.local_level = local_level;
	}
	public float getRain_fall() {
		return rain_fall;
	}
	public void setRain_fall(float rain_fall) {
		this.rain_fall = rain_fall;
	}
	
	@Override
	public String toString() {
		return "WaterlevelrainInfo [id=" + id + ", station_id=" + station_id + ", save_time=" + save_time
				+ ", out_level=" + out_level + ", local_level=" + local_level + ", rain_fall=" + rain_fall + "]";
	}
	
	
}
