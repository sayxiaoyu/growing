package com.lj.domain;

import java.sql.Timestamp;

public class OfflineInfo {

	private int id;//
	
	private int station_id;
	private Timestamp offline_time;//离线时间
	private int device_no;//设备编号,0-全部离线
	private Timestamp online_time;//修复时间
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStation_id() {
		return station_id;
	}
	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}
	public Timestamp getOffline_time() {
		return offline_time;
	}
	public void setOffline_time(Timestamp offline_time) {
		this.offline_time = offline_time;
	}
	public int getDevice_no() {
		return device_no;
	}
	public void setDevice_no(int device_no) {
		this.device_no = device_no;
	}
	public Timestamp getOnline_time() {
		return online_time;
	}
	public void setOnline_time(Timestamp online_time) {
		this.online_time = online_time;
	}
	@Override
	public String toString() {
		return "OfflineInfo [id=" + id + ", station_id=" + station_id + ", offline_time=" + offline_time
				+ ", device_no=" + device_no + ", online_time=" + online_time + "]";
	}
	
	
}
