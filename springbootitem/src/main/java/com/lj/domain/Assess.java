package com.lj.domain;

import java.sql.Timestamp;

public class Assess {
	
	private int id;//自增id
	private int household_num;//实收农户数量
	private int estimate_num;//应收农户数量
	private String staff_id;//收运员身份证号
	private int collector_id;//智能采收终端编号
	private Timestamp collect_stime;//当天采收开始时间
	private Timestamp collect_etime;//当天采收结束时间
	private Timestamp collect_ftime;//收运员完成当天80%收运量时间
	private double kitchen_weight;//厨余垃圾总量
	private double other_weight;//其他垃圾总量
	private double kitchen_num;//易腐垃圾采收农户数
	private double kitchen_qualify;//易腐垃圾分类合格农户数
	private double sampling_num;//抽检易腐垃圾农户数
	private double sampling_qualify;//易腐垃圾抽检合格农户数
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getHousehold_num() {
		return household_num;
	}
	public void setHousehold_num(int household_num) {
		this.household_num = household_num;
	}
	public int getEstimate_num() {
		return estimate_num;
	}
	public void setEstimate_num(int estimate_num) {
		this.estimate_num = estimate_num;
	}
	public String getStaff_id() {
		return staff_id;
	}
	public void setStaff_id(String staff_id) {
		this.staff_id = staff_id;
	}
	public int getCollector_id() {
		return collector_id;
	}
	public void setCollector_id(int collector_id) {
		this.collector_id = collector_id;
	}
	public Timestamp getCollect_stime() {
		return collect_stime;
	}
	public void setCollect_stime(Timestamp collect_stime) {
		this.collect_stime = collect_stime;
	}
	public Timestamp getCollect_etime() {
		return collect_etime;
	}
	public void setCollect_etime(Timestamp collect_etime) {
		this.collect_etime = collect_etime;
	}
	public Timestamp getCollect_ftime() {
		return collect_ftime;
	}
	public void setCollect_ftime(Timestamp collect_ftime) {
		this.collect_ftime = collect_ftime;
	}
	public double getKitchen_weight() {
		return kitchen_weight;
	}
	public void setKitchen_weight(double kitchen_weight) {
		this.kitchen_weight = kitchen_weight;
	}
	public double getOther_weight() {
		return other_weight;
	}
	public void setOther_weight(double other_weight) {
		this.other_weight = other_weight;
	}
	public double getKitchen_num() {
		return kitchen_num;
	}
	public void setKitchen_num(double kitchen_num) {
		this.kitchen_num = kitchen_num;
	}
	public double getKitchen_qualify() {
		return kitchen_qualify;
	}
	public void setKitchen_qualify(double kitchen_qualify) {
		this.kitchen_qualify = kitchen_qualify;
	}
	public double getSampling_num() {
		return sampling_num;
	}
	public void setSampling_num(double sampling_num) {
		this.sampling_num = sampling_num;
	}
	public double getSampling_qualify() {
		return sampling_qualify;
	}
	public void setSampling_qualify(double sampling_qualify) {
		this.sampling_qualify = sampling_qualify;
	}
	@Override
	public String toString() {
		return "Assess [id=" + id + ", household_num=" + household_num + ", estimate_num=" + estimate_num
				+ ", staff_id=" + staff_id + ", collector_id=" + collector_id + ", collect_stime=" + collect_stime
				+ ", collect_etime=" + collect_etime + ", collect_ftime=" + collect_ftime + ", kitchen_weight="
				+ kitchen_weight + ", other_weight=" + other_weight + ", kitchen_num=" + kitchen_num
				+ ", kitchen_qualify=" + kitchen_qualify + ", sampling_num=" + sampling_num + ", sampling_qualify="
				+ sampling_qualify + "]";
	}
	
	
	
}
