package com.lj.domain;

import java.util.Date;


public class PumpingstationInfo {
	
	private int no;//
	
	private String pumpingstationname;//电排站名称
	private int waterpump1;//水泵1状态
	private int waterpump2;//水泵2状态
	private int waterpump3;//水泵3状态
	private int waterpump4;//水泵4状态
	private int waterpump5;//水泵5状态
	private int waterpump6;//水泵6状态
	private int waterpump7;//水泵7状态
	private int waterpump8;//水泵8状态
	private int waterpump9;//水泵9状态
	private int waterpump10;//水泵10状态
	private int waterpump11;//水泵11状态
	private int waterpump12;//水泵12状态
	private int waterpump13;//水泵13状态
	private int waterpump14;//水泵14状态
	private int watergate1;//闸门1状态
	private int watergate2;//闸门2状态
	private int watergate3;//闸门3状态
	private int watergate4;//闸门4状态
	private float rainfall;//雨量
	private float inwaterlevel;//外池水量
	private float outwaterlevel;//外江水量
	private Date updatetime;//更新时间
	private String others;//其他
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getPumpingstationname() {
		return pumpingstationname;
	}
	public void setPumpingstationname(String pumpingstationname) {
		this.pumpingstationname = pumpingstationname;
	}
	public int getWaterpump1() {
		return waterpump1;
	}
	public void setWaterpump1(int waterpump1) {
		this.waterpump1 = waterpump1;
	}
	public int getWaterpump2() {
		return waterpump2;
	}
	public void setWaterpump2(int waterpump2) {
		this.waterpump2 = waterpump2;
	}
	public int getWaterpump3() {
		return waterpump3;
	}
	public void setWaterpump3(int waterpump3) {
		this.waterpump3 = waterpump3;
	}
	public int getWaterpump4() {
		return waterpump4;
	}
	public void setWaterpump4(int waterpump4) {
		this.waterpump4 = waterpump4;
	}
	public int getWaterpump5() {
		return waterpump5;
	}
	public void setWaterpump5(int waterpump5) {
		this.waterpump5 = waterpump5;
	}
	public int getWaterpump6() {
		return waterpump6;
	}
	public void setWaterpump6(int waterpump6) {
		this.waterpump6 = waterpump6;
	}
	public int getWaterpump7() {
		return waterpump7;
	}
	public void setWaterpump7(int waterpump7) {
		this.waterpump7 = waterpump7;
	}
	public int getWaterpump8() {
		return waterpump8;
	}
	public void setWaterpump8(int waterpump8) {
		this.waterpump8 = waterpump8;
	}
	public int getWaterpump9() {
		return waterpump9;
	}
	public void setWaterpump9(int waterpump9) {
		this.waterpump9 = waterpump9;
	}
	public int getWaterpump10() {
		return waterpump10;
	}
	public void setWaterpump10(int waterpump10) {
		this.waterpump10 = waterpump10;
	}
	public int getWaterpump11() {
		return waterpump11;
	}
	public void setWaterpump11(int waterpump11) {
		this.waterpump11 = waterpump11;
	}
	public int getWaterpump12() {
		return waterpump12;
	}
	public void setWaterpump12(int waterpump12) {
		this.waterpump12 = waterpump12;
	}
	public int getWaterpump13() {
		return waterpump13;
	}
	public void setWaterpump13(int waterpump13) {
		this.waterpump13 = waterpump13;
	}
	public int getWaterpump14() {
		return waterpump14;
	}
	public void setWaterpump14(int waterpump14) {
		this.waterpump14 = waterpump14;
	}
	public int getWatergate1() {
		return watergate1;
	}
	public void setWatergate1(int watergate1) {
		this.watergate1 = watergate1;
	}
	public int getWatergate2() {
		return watergate2;
	}
	public void setWatergate2(int watergate2) {
		this.watergate2 = watergate2;
	}
	public int getWatergate3() {
		return watergate3;
	}
	public void setWatergate3(int watergate3) {
		this.watergate3 = watergate3;
	}
	public int getWatergate4() {
		return watergate4;
	}
	public void setWatergate4(int watergate4) {
		this.watergate4 = watergate4;
	}
	public float getRainfall() {
		return rainfall;
	}
	public void setRainfall(float rainfall) {
		this.rainfall = rainfall;
	}
	public float getInwaterlevel() {
		return inwaterlevel;
	}
	public void setInwaterlevel(float inwaterlevel) {
		this.inwaterlevel = inwaterlevel;
	}
	public float getOutwaterlevel() {
		return outwaterlevel;
	}
	public void setOutwaterlevel(float outwaterlevel) {
		this.outwaterlevel = outwaterlevel;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	
	@Override
	public String toString() {
		return "PumpingstationInfo [no=" + no + ", pumpingstationname=" + pumpingstationname + ", waterpump1="
				+ waterpump1 + ", waterpump2=" + waterpump2 + ", waterpump3=" + waterpump3 + ", waterpump4="
				+ waterpump4 + ", waterpump5=" + waterpump5 + ", waterpump6=" + waterpump6 + ", waterpump7="
				+ waterpump7 + ", waterpump8=" + waterpump8 + ", waterpump9=" + waterpump9 + ", waterpump10="
				+ waterpump10 + ", waterpump11=" + waterpump11 + ", waterpump12=" + waterpump12 + ", waterpump13="
				+ waterpump13 + ", waterpump14=" + waterpump14 + ", watergate1=" + watergate1 + ", watergate2="
				+ watergate2 + ", watergate3=" + watergate3 + ", watergate4=" + watergate4 + ", rainfall=" + rainfall
				+ ", inwaterlevel=" + inwaterlevel + ", outwaterlevel=" + outwaterlevel + ", updatetime=" + updatetime
				+ ", others=" + others + "]";
	}
	
	
}
