package com.lj.domain;

import java.sql.Timestamp;


public class ContinuetimeInfo {
	
	private int id;//
	
	private int station_id;
	private Timestamp open_time;//打开时间
	private Timestamp close_time;//关闭时间
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStation_id() {
		return station_id;
	}
	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}
	public Timestamp getOpen_time() {
		return open_time;
	}
	public void setOpen_time(Timestamp open_time) {
		this.open_time = open_time;
	}
	public Timestamp getClose_time() {
		return close_time;
	}
	public void setClose_time(Timestamp close_time) {
		this.close_time = close_time;
	}
	@Override
	public String toString() {
		return "ContinuetimeInfo [id=" + id + ", station_id=" + station_id + ", open_time=" + open_time
				+ ", close_time=" + close_time + "]";
	}
	
	
}
