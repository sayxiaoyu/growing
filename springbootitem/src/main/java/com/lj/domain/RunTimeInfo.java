package com.lj.domain;



public class RunTimeInfo {
	
	private int id;//
	
	private int station_id;
	private int run_time;//运行时间(多少分钟)
	private String adddate;//日期
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStation_id() {
		return station_id;
	}
	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}
	public int getRun_time() {
		return run_time;
	}
	public void setRun_time(int run_time) {
		this.run_time = run_time;
	}
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
	@Override
	public String toString() {
		return "RunTimeInfo [id=" + id + ", station_id=" + station_id + ", run_time=" + run_time + ", adddate="
				+ adddate + "]";
	}
	
	
}
