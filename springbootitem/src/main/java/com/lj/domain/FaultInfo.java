package com.lj.domain;

import java.sql.Timestamp;

public class FaultInfo {
	

	private int id;//
	
	private int station_id;//站点编号
	private Timestamp fault_time;//故障时间
	private int device_type;//设备类型0-水泵 1-闸门
	private int device_no;//设备编号
	private Timestamp solve_time;//修复时间
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStation_id() {
		return station_id;
	}
	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}
	public Timestamp getFault_time() {
		return fault_time;
	}
	public void setFault_time(Timestamp fault_time) {
		this.fault_time = fault_time;
	}
	public int getDevice_type() {
		return device_type;
	}
	public void setDevice_type(int device_type) {
		this.device_type = device_type;
	}
	public int getDevice_no() {
		return device_no;
	}
	public void setDevice_no(int device_no) {
		this.device_no = device_no;
	}
	public Timestamp getSolve_time() {
		return solve_time;
	}
	public void setSolve_time(Timestamp solve_time) {
		this.solve_time = solve_time;
	}
	@Override
	public String toString() {
		return "FaultInfo [id=" + id + ", station_id=" + station_id + ", fault_time=" + fault_time + ", device_type="
				+ device_type + ", device_no=" + device_no + ", solve_time=" + solve_time + "]";
	}
	
	
}
