package com.lj.domain;

public class Advertisement {
	
	private int id;
	private String ad_photo;
	private String collector_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAd_photo() {
		return ad_photo;
	}
	public void setAd_photo(String ad_photo) {
		this.ad_photo = ad_photo;
	}
	public String getCollector_id() {
		return collector_id;
	}
	public void setCollector_id(String collector_id) {
		this.collector_id = collector_id;
	}
	@Override
	public String toString() {
		return "Advertisement [id=" + id + ", ad_photo=" + ad_photo + ", collector_id=" + collector_id + "]";
	}
	
	
	
	
	
}
