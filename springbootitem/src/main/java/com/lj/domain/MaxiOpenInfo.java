package com.lj.domain;

public class MaxiOpenInfo {

	private int id;//
	
	private int station_id;//
	private int maxi_open;//最大开机数
	private String datetime;//日期
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStation_id() {
		return station_id;
	}
	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}
	public int getMaxi_open() {
		return maxi_open;
	}
	public void setMaxi_open(int maxi_open) {
		this.maxi_open = maxi_open;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	@Override
	public String toString() {
		return "MaxiOpenInfo [id=" + id + ", station_id=" + station_id + ", maxi_open=" + maxi_open + ", datetime="
				+ datetime + "]";
	}
	
	
}
