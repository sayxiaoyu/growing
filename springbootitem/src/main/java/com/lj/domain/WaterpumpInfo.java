package com.lj.domain;

import java.sql.Timestamp;

public class WaterpumpInfo {
	
	private int id;//
	
	private int station_id;//
	private Timestamp open_time;//打开时间
	private Timestamp close_time;//关闭时间
	private int device_no;//设备编号
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStation_id() {
		return station_id;
	}
	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}
	public Timestamp getOpen_time() {
		return open_time;
	}
	public void setOpen_time(Timestamp open_time) {
		this.open_time = open_time;
	}
	public Timestamp getClose_time() {
		return close_time;
	}
	public void setClose_time(Timestamp close_time) {
		this.close_time = close_time;
	}
	public int getDevice_no() {
		return device_no;
	}
	public void setDevice_no(int device_no) {
		this.device_no = device_no;
	}
	@Override
	public String toString() {
		return "WaterpumpInfo [id=" + id + ", station_id=" + station_id + ", open_time=" + open_time + ", close_time="
				+ close_time + ", device_no=" + device_no + "]";
	}
	
	
}
