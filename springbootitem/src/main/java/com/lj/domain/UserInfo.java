package com.lj.domain;

public class UserInfo {

	private int id;
	
	private String loginname;//登录账号
	private String password;//密码
	private String username;//用户名称
	private int userclass;//级别
	private String region_id;//区域编码，县市：6位；镇乡：县市代码+3位；行政村：镇乡代码+3位 按照国家标准执行
	private String sex;//性别

	/**
	 * Getter method for property<tt>id</tt>.
	 *
	 * @return property value of id
	 */
	public int getId() {return id;}

	/**
	 * Setter method for property <tt>id</tt>.
	 *
	 * @param id value to be assigned to property id
	 */
	public void setId(intid) {this.id = id;}

	/**
	 * Getter method for property<tt>loginname</tt>.
	 *
	 * @return property value of loginname
	 */
	public String getLoginname() {return loginname;}

	/**
	 * Setter method for property <tt>loginname</tt>.
	 *
	 * @param loginname value to be assigned to property loginname
	 */
	public void
	setLoginname(Stringloginname) {this.loginname = loginname;}

	/**
	 * Getter method for property<tt>password</tt>.
	 *
	 * @return property value of password
	 */
	public String getPassword() {return password;}

	/**
	 * Setter method for property <tt>password</tt>.
	 *
	 * @param password value to be assigned to property password
	 */
	public void setPassword(Stringpassword) {this.password = password;}

	/**
	 * Getter method for property<tt>username</tt>.
	 *
	 * @return property value of username
	 */
	public String getUsername() {return username;}

	/**
	 * Setter method for property <tt>username</tt>.
	 *
	 * @param username value to be assigned to property username
	 */
	public void setUsername(Stringusername) {this.username = username;}

	/**
	 * Getter method for property<tt>userclass</tt>.
	 *
	 * @return property value of userclass
	 */
	public int getUserclass() {return userclass;}

	/**
	 * Setter method for property <tt>userclass</tt>.
	 *
	 * @param userclass value to be assigned to property userclass
	 */
	public void setUserclass(intuserclass) {this.userclass = userclass;}

	/**
	 * Getter method for property<tt>region_id</tt>.
	 *
	 * @return property value of region_id
	 */
	public String getRegion_id() {return region_id;}

	/**
	 * Setter method for property <tt>region_id</tt>.
	 *
	 * @param region_id value to be assigned to property region_id
	 */
	public void setRegion_id(Stringregion_id) {this.region_id = region_id;}

	/**
	 * Getter method for property<tt>sex</tt>.
	 *
	 * @return property value of sex
	 */
	public String getSex() {return sex;}

	/**
	 * Setter method for property <tt>sex</tt>.
	 *
	 * @param sex value to be assigned to property sex
	 */
	public void setSex(Stringsex) {this.sex = sex;}

}
