package com.lj.domain;

import java.sql.Timestamp;

public class OperateInfo {
	
	private int id;//
	
	private int station_id;//
	private Timestamp operate_time;//操作时间
	private int device_type;//设备类型0-水泵 1-闸门
	private int device_no;//设备编号
	private int oprate_type;//操作类型0-停止 1-打开
	private String user_id;//操作账号
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStation_id() {
		return station_id;
	}
	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}
	public Timestamp getOperate_time() {
		return operate_time;
	}
	public void setOperate_time(Timestamp operate_time) {
		this.operate_time = operate_time;
	}
	public int getDevice_type() {
		return device_type;
	}
	public void setDevice_type(int device_type) {
		this.device_type = device_type;
	}
	public int getDevice_no() {
		return device_no;
	}
	public void setDevice_no(int device_no) {
		this.device_no = device_no;
	}
	public int getOprate_type() {
		return oprate_type;
	}
	public void setOprate_type(int oprate_type) {
		this.oprate_type = oprate_type;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	@Override
	public String toString() {
		return "OperateInfo [id=" + id + ", station_id=" + station_id + ", operate_time=" + operate_time
				+ ", device_type=" + device_type + ", device_no=" + device_no + ", oprate_type=" + oprate_type
				+ ", user_id=" + user_id + "]";
	}
	
	
}
