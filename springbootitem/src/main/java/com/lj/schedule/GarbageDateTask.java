/*
 * package com.lj.schedule;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.scheduling.annotation.Scheduled; import
 * org.springframework.stereotype.Component;
 * 
 * import rubbishitem.service.AssessService; import
 * rubbishitem.service.DGarbageService; import
 * rubbishitem.service.HGarbageService; import
 * rubbishitem.service.SamplingService;
 * 
 * @Component public class GarbageDateTask {
 * 
 * @Autowired private DGarbageService dGarbageService;
 * 
 * @Autowired private HGarbageService hGarbageService;
 * 
 * @Autowired private AssessService assessService;
 * 
 * @Autowired private SamplingService samplingService; //0 0 23 * * ? 每晚23点
 * // @Scheduled(cron = "0/5 * *  * * ?") "0 0 22 * * ?"
 * 
 * 1.生成当天的assess数据 2.生成当天的sampling数据 3.将dgarbage的表中数据迁移到hgarbage
 * 
 * @Scheduled(cron = "0 0 22 * * ?") public void garbagesynchro1() {
 * //添加assess数据 assessService.getAssess(); //添加抽检数据
 * samplingService.addSampling(); // 将数据导入到历史表 hGarbageService.addHGarbage(); //
 * 删除dgarbage表中数据 dGarbageService.deleteDGgarbage(); }
 * 
 * //@Scheduled(cron = "0 30 22 * * ?") // public void garbagesynchro2() {
 * 
 * // }
 * 
 * // @Scheduled(cron = "0 0 23 * * ?") // public void garbagesynchro3() {
 * 
 * //}
 * 
 * }
 */