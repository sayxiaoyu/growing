package com.lj.comm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    
	/*
	 * @Bean public Docket createRestApi() { return new
	 * Docket(DocumentationType.SWAGGER_2) .pathMapping("/") .select()
	 * .apis(RequestHandlerSelectors.basePackage("com.lj.controller"))
	 * .paths(PathSelectors.any()) .build().apiInfo(new ApiInfoBuilder()
	 * .title("SpringBoot整合Swagger") .description("SpringBoot整合Swagger，详细信息......")
	 * .version("9.0") .contact(new
	 * Contact("整合好了，嘻嘻","blog.csdn.net","aaa@gmail.com"))
	 * .license("The Apache License") .licenseUrl("http://www.baidu.com") .build());
	 * }
	 */
	
	@Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                // 自行修改为自己的包路径
                .apis(RequestHandlerSelectors.basePackage("com.lj.web"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("农村生活垃圾接口文档")
                .description("生活垃圾管理中心 API 1.0 操作文档")
                //服务条款网址
                .termsOfServiceUrl("https://shimo.im/docs/PKVxX8rYHDQXqKhg/read")
                .version("1.0")
                .contact(new Contact("lj", "https://shimo.im/docs/PKVxX8rYHDQXqKhg/read", "zxy506330625@vip.qq.com"))
                .build();
    }
	
}
