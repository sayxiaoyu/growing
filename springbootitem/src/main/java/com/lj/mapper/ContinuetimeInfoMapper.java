package com.lj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.lj.domain.ContinuetimeInfo;

public interface ContinuetimeInfoMapper {
	
	@Select("select * from continuetime_info where username like concat('%',#{name},'%')")
    List<ContinuetimeInfo> getUsersByName(String name);
	
}
