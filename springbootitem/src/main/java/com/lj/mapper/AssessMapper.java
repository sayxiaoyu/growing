package com.lj.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Select;


public interface AssessMapper {
	
	@Select("select 	\r\n" + 
			"            	sum(household_num) /(datediff(date_format(max(collect_stime),'%Y-%m-%d') ,date_format(min(collect_stime),'%Y-%m-%d'))+1) as avg_household_num, \r\n" + 
			"				sum(kitchen_num)/(datediff(date_format(max(collect_stime),'%Y-%m-%d') ,date_format(min(collect_stime),'%Y-%m-%d'))+1) as avg_kitchen_num,   \r\n" + 
			"				sum(other_weight) as other_weight,sum(kitchen_weight) as kitchen_weight, \r\n" + 
			"				sum(kitchen_weight)/(sum(kitchen_weight)+sum(other_weight)) as decrease_rate, \r\n" + 
			"				sum(other_weight)/sum(household_num) as avg_other_weight,sum(kitchen_weight)/sum(kitchen_num) as avg_kitchen_weight, \r\n" + 
			"				sum(kitchen_qualify)/sum(kitchen_num) as avg_kitchen_qualify,   \r\n" + 
			"				sum(household_num)/((datediff(date_format(max(collect_stime),'%Y-%m-%d') ,date_format(min(collect_stime),'%Y-%m-%d'))+1)*(select count(household_id) from lj_household \r\n" + 
			"				where left(household_id,length(#{regionid}))=#{regionid})) as avg_local_rate,\r\n" + 
			"				floor(avg(date_format(collect_stime,'%k'))) as avg_stime,\r\n" + 
			"				ceil(avg(date_format(collect_etime,'%k')))  as avg_etime, \r\n" + 
			"				round(avg(date_format(collect_ftime,'%k')))  as avg_ftime, \r\n" + 
			"				ifnull((sum(sampling_qualify)/sum(sampling_num)),0) as collect_qualify,\r\n" + 
			"				ifnull((sum(household_num)/(avg(date_format(collect_etime,'%k')) - avg(date_format(collect_stime,'%k')))),sum(household_num)) as collect_efficiency, \r\n" + 
			"				if (sum(household_num)/sum(estimate_num)>1, 1, sum(household_num)/sum(estimate_num)) as avg_collect_rate  \r\n" + 
			"		from lj_assess \r\n" + 
			"		where left(collector_id,length(#{regionid}))=#{regionid} and (date_format(collect_stime,'%Y-%m-%d') between date_format(#{sdate},'%Y-%m-%d')  and date_format(#{edate},'%Y-%m-%d'))")
	Map<String,Object> getRegionKPI(String regionid,String sdate,String edate);
	
	@Select("select 	\r\n" + 
			"            	sum(household_num) /(datediff(date_format(max(collect_stime),'%Y-%m-%d') ,date_format(min(collect_stime),'%Y-%m-%d'))+1) as avg_household_num, \r\n" + 
			"				sum(kitchen_num)/(datediff(date_format(max(collect_stime),'%Y-%m-%d') ,date_format(min(collect_stime),'%Y-%m-%d'))+1) as avg_kitchen_num,   \r\n" + 
			"				sum(other_weight) as other_weight,sum(kitchen_weight) as kitchen_weight, \r\n" + 
			"				sum(kitchen_weight)/(sum(kitchen_weight)+sum(other_weight)) as decrease_rate, \r\n" + 
			"				sum(other_weight)/sum(household_num) as avg_other_weight,sum(kitchen_weight)/sum(kitchen_num) as avg_kitchen_weight, \r\n" + 
			"				sum(kitchen_qualify)/sum(kitchen_num) as avg_kitchen_qualify,   \r\n" + 
			"				sum(household_num)/((datediff(date_format(max(collect_stime),'%Y-%m-%d') ,date_format(min(collect_stime),'%Y-%m-%d'))+1)*(select count(household_id) from lj_household \r\n" + 
			"				where left(household_id,length(#{regionid}))=#{regionid})) as avg_local_rate,\r\n" + 
			"				floor(avg(date_format(collect_stime,'%k'))) as avg_stime,\r\n" + 
			"				ceil(avg(date_format(collect_etime,'%k')))  as avg_etime, \r\n" + 
			"				round(avg(date_format(collect_ftime,'%k')))  as avg_ftime, \r\n" + 
			"				ifnull((sum(sampling_qualify)/sum(sampling_num)),0) as collect_qualify,\r\n" + 
			"				ifnull((sum(household_num)/(avg(date_format(collect_etime,'%k')) - avg(date_format(collect_stime,'%k')))),sum(household_num)) as collect_efficiency, \r\n" + 
			"				if (sum(household_num)/sum(estimate_num)>1, 1, sum(household_num)/sum(estimate_num)) as avg_collect_rate  \r\n" + 
			"		from lj_assess \r\n" + 
			"		where left(collector_id,length(#{regionid}))=#{regionid} and (date_format(collect_stime,'%Y-%m-%d') between date_format(#{sdate},'%Y-%m-%d')  and date_format(#{edate},'%Y-%m-%d'))")
	Map<String,Object> getRegionDate(String regionid,String sdate,String edate);
}
