package com.lj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import com.lj.domain.UserInfo;

public interface UserMapper {
	
	@Select("select * from lj_userinfo where username like concat('%',#{name},'%')")
    List<UserInfo> getUsersByName(String name);
	
	@Select({"select * from lj_userinfo where left(household_id,12) = #{househhold}"})
	@Results(id="studentMap", value={
	    @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
	    @Result(column="loginname", property="loginname", jdbcType=JdbcType.VARCHAR),
	    @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR)
	})
	List<UserInfo> selectAll(String household_id);
	
	
}
