package com.lj.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.lj.domain.Advertisement;

@Repository
public interface AdvertisementMapper {
	
	public List<Advertisement> getADByCollectorID(@Param("collector_id")String collector_id);
}
