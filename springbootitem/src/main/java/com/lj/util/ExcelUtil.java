package com.lj.util;
/**
 * 
 */
/*
 * package rubbishitem.util;
 * 
 * import java.io.File; import java.io.FileInputStream; import
 * java.io.FileOutputStream; import java.io.IOException; import
 * java.io.InputStream; import java.io.OutputStream; import
 * java.text.DateFormat; import java.text.SimpleDateFormat; import
 * java.util.ArrayList; import java.util.Date; import java.util.List;
 * 
 * import org.apache.poi.hssf.usermodel.HSSFCell; import
 * org.apache.poi.hssf.usermodel.HSSFRichTextString; import
 * org.apache.poi.hssf.usermodel.HSSFRow; import
 * org.apache.poi.hssf.usermodel.HSSFSheet; import
 * org.apache.poi.hssf.usermodel.HSSFWorkbook; import
 * org.apache.poi.xssf.usermodel.XSSFCell; import
 * org.apache.poi.xssf.usermodel.XSSFRichTextString; import
 * org.apache.poi.xssf.usermodel.XSSFRow; import
 * org.apache.poi.xssf.usermodel.XSSFSheet; import
 * org.apache.poi.xssf.usermodel.XSSFWorkbook;
 * 
 * import rubbishitem.common.Common; import rubbishitem.pojo.ImportAndExport;
 * import rubbishitem.pojo.Student;
 * 
 *//**
	 * @author ztk
	 * @created 2019-9-7
	 */
/*
 * public class ExcelUtil {
 * 
 * public void writeExcel(List<ImportAndExport> list, String path) throws
 * Exception { if (list == null) { return; } else if (path == null ||
 * Common.EMPTY.equals(path)) { return; } else { String postfix =
 * Util.getPostfix(path); if (!Common.EMPTY.equals(postfix)) { if
 * (Common.OFFICE_EXCEL_2003_POSTFIX.equals(postfix)) { writeXls(list, path); }
 * else if (Common.OFFICE_EXCEL_2010_POSTFIX.equals(postfix)) { writeXlsx(list,
 * path); } }else{ System.out.println(path + Common.NOT_EXCEL_FILE); } } }
 * 
 *//**
	 * read the Excel file
	 * 
	 * @param path the path of the Excel file
	 * @return
	 * @throws IOException
	 */
/*
 * public List<ImportAndExport> readExcel(String path) throws Exception { if
 * (path == null || Common.EMPTY.equals(path)) { return null; } else { String
 * postfix = Util.getPostfix(path);//判断是xls还是xlsx if
 * (!Common.EMPTY.equals(postfix)) { if
 * (Common.OFFICE_EXCEL_2003_POSTFIX.equals(postfix)) { return readXls(path); }
 * else if (Common.OFFICE_EXCEL_2010_POSTFIX.equals(postfix)) { return
 * readXlsx(path); } } else { System.out.println(path + Common.NOT_EXCEL_FILE);
 * } } return null; }
 * 
 *//**
	 * Read the Excel 2010
	 * 
	 * @param path the path of the excel file
	 * @return
	 * @throws IOException
	 */
/*
 * public List<ImportAndExport> readXlsx(String path) throws Exception {
 * InputStream is = new FileInputStream(path); XSSFWorkbook xssfWorkbook = new
 * XSSFWorkbook(is); ImportAndExport importAndExport = null;
 * List<ImportAndExport> list = new ArrayList<ImportAndExport>(); // Read the
 * Sheet for (int numSheet = 0; numSheet < xssfWorkbook.getNumberOfSheets();
 * numSheet++) { XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(numSheet); if
 * (xssfSheet == null) { continue; } // Read the Row for (int rowNum = 1; rowNum
 * <= xssfSheet.getLastRowNum(); rowNum++) { XSSFRow xssfRow =
 * xssfSheet.getRow(rowNum); if (xssfRow != null) { importAndExport = new
 * ImportAndExport(); XSSFCell contractnumber = xssfRow.getCell(0); XSSFCell
 * partners = xssfRow.getCell(1); XSSFCell contractname = xssfRow.getCell(2);
 * XSSFCell clerk = xssfRow.getCell(3); XSSFCell contractdetails =
 * xssfRow.getCell(4); XSSFCell totalamount = xssfRow.getCell(5); XSSFCell
 * auditamount = xssfRow.getCell(6); XSSFCell signtime = xssfRow.getCell(7);
 * 
 * importAndExport.setId(rowNum); if(auditamount == null) {
 * importAndExport.setAuditamount("0"); }else {
 * importAndExport.setAuditamount(getValue(auditamount)); } if(clerk == null) {
 * importAndExport.setClerk("�?"); }else {
 * importAndExport.setClerk(getValue(clerk)); } if(contractdetails == null) {
 * importAndExport.setContractdetails(""); }else {
 * importAndExport.setContractdetails(getValue(contractdetails)); }
 * if(contractname == null) { importAndExport.setContractname("�?"); }else {
 * importAndExport.setContractname(getValue(contractname)); } if(contractnumber
 * == null) { importAndExport.setContractnumber("�?"); }else {
 * importAndExport.setContractnumber(getValue(contractnumber)); } if(partners ==
 * null) { importAndExport.setPartners("�?"); }else {
 * importAndExport.setPartners(getValue(partners)); } if(totalamount == null) {
 * importAndExport.setTotalamount("0"); }else {
 * importAndExport.setTotalamount(getValue(totalamount)); } if(signtime == null)
 * { Date date = new Date(); SimpleDateFormat sdf = new
 * SimpleDateFormat("yyyy-MM-dd");
 * importAndExport.setSigntime(sdf.format(date)); }else { Date d =
 * signtime.getDateCellValue(); DateFormat formater = new
 * SimpleDateFormat("yyyy-MM-dd"); String signtime1 = formater.format(d);
 * 
 * //System.out.println(signtime1); importAndExport.setSigntime(signtime1); }
 * 
 * 
 * list.add(importAndExport); } } } return list; }
 * 
 *//**
	 * Read the Excel 2003-2007
	 * 
	 * @param path the path of the Excel
	 * @return
	 * @throws IOException
	 *//*
		 * public List<ImportAndExport> readXls(String path) throws IOException {
		 * //System.out.println(Common.PROCESSING + path); InputStream is = new
		 * FileInputStream(path); HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
		 * ImportAndExport importAndExport = null; List<ImportAndExport> list = new
		 * ArrayList<ImportAndExport>(); // Read the Sheet for (int numSheet = 0;
		 * numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) { HSSFSheet
		 * hssfSheet = hssfWorkbook.getSheetAt(numSheet); if (hssfSheet == null) {
		 * continue; } // Read the Row for (int rowNum = 1; rowNum <=
		 * hssfSheet.getLastRowNum(); rowNum++) { HSSFRow hssfRow =
		 * hssfSheet.getRow(rowNum); if (hssfRow != null) { importAndExport = new
		 * ImportAndExport(); HSSFCell contractnumber = hssfRow.getCell(0); HSSFCell
		 * partners = hssfRow.getCell(1); HSSFCell contractname = hssfRow.getCell(2);
		 * HSSFCell clerk = hssfRow.getCell(3); HSSFCell contractdetails =
		 * hssfRow.getCell(4); HSSFCell totalamount = hssfRow.getCell(5); HSSFCell
		 * auditamount = hssfRow.getCell(6); HSSFCell signtime = hssfRow.getCell(7);
		 * 
		 * importAndExport.setId(rowNum); if(auditamount == null) {
		 * importAndExport.setAuditamount("0"); }else {
		 * importAndExport.setAuditamount(getValue(auditamount)); } if(clerk == null) {
		 * importAndExport.setClerk("�?"); }else {
		 * importAndExport.setClerk(getValue(clerk)); } if(contractdetails == null) {
		 * importAndExport.setContractdetails(""); }else {
		 * importAndExport.setContractdetails(getValue(contractdetails)); }
		 * if(contractname == null) { importAndExport.setContractname("�?"); }else {
		 * importAndExport.setContractname(getValue(contractname)); } if(contractnumber
		 * == null) { importAndExport.setContractnumber("�?"); }else {
		 * importAndExport.setContractnumber(getValue(contractnumber)); } if(partners ==
		 * null) { importAndExport.setPartners("�?"); }else {
		 * importAndExport.setPartners(getValue(partners)); } if(totalamount == null) {
		 * importAndExport.setTotalamount("0"); }else {
		 * importAndExport.setTotalamount(getValue(totalamount)); } if(signtime == null)
		 * { Date date = new Date(); SimpleDateFormat sdf = new
		 * SimpleDateFormat("yyyy-MM-dd");
		 * importAndExport.setSigntime(sdf.format(date)); }else { Date d =
		 * signtime.getDateCellValue(); DateFormat formater = new
		 * SimpleDateFormat("yyyy-MM-dd"); String signtime1 = formater.format(d);
		 * 
		 * //System.out.println(signtime1); importAndExport.setSigntime(signtime1); }
		 * 
		 * list.add(importAndExport); } } } return list; }
		 * 
		 * @SuppressWarnings("static-access") private String getValue(XSSFCell xssfRow)
		 * { if (xssfRow.getCellType() == xssfRow.CELL_TYPE_BOOLEAN) { return
		 * String.valueOf(xssfRow.getBooleanCellValue()); } else if
		 * (xssfRow.getCellType() == xssfRow.CELL_TYPE_NUMERIC) { return
		 * String.valueOf(xssfRow.getNumericCellValue()); } else { return
		 * String.valueOf(xssfRow.getStringCellValue()); } }
		 * 
		 * @SuppressWarnings("static-access") private String getValue(HSSFCell hssfCell)
		 * { if (hssfCell.getCellType() == hssfCell.CELL_TYPE_BOOLEAN) { return
		 * String.valueOf(hssfCell.getBooleanCellValue()); } else if
		 * (hssfCell.getCellType() == hssfCell.CELL_TYPE_NUMERIC) { return
		 * String.valueOf(hssfCell.getNumericCellValue()); } else { return
		 * String.valueOf(hssfCell.getStringCellValue()); } }
		 * 
		 * public void writeXls(List<ImportAndExport> list, String path) throws
		 * Exception { if (list == null) { return; } int countColumnNum = list.size();
		 * HSSFWorkbook book = new HSSFWorkbook(); HSSFSheet sheet =
		 * book.createSheet("contractsheet"); // option at first row. HSSFRow firstRow =
		 * sheet.createRow(0); HSSFCell[] firstCells = new HSSFCell[countColumnNum];
		 * String[] options = { "合同编号", "客户公司名称", "合同名称", "业务�?", "合同备注", "合同金额",
		 * "审计金额", "收款金额", "未收金额", "款项内容","签订日期" }; System.out.println(options.length);
		 * for (int j = 0; j < options.length; j++) { firstCells[0] =
		 * firstRow.createCell(j); firstCells[0].setCellValue(new
		 * HSSFRichTextString(options[j])); } // for (int i = 0; i < countColumnNum;
		 * i++) { HSSFRow row = sheet.createRow(i + 1); ImportAndExport importAndExport
		 * = list.get(i); for (int column = 0; column < options.length; column++) {
		 * HSSFCell contractnumber = row.createCell(0); HSSFCell partners =
		 * row.createCell(1); HSSFCell contractname = row.createCell(2); HSSFCell clerk
		 * = row.createCell(3); HSSFCell contractdetails = row.createCell(4); HSSFCell
		 * totalamount = row.createCell(5); HSSFCell auditamount = row.createCell(6);
		 * HSSFCell received = row.createCell(7); HSSFCell balance = row.createCell(8);
		 * HSSFCell fundcontent = row.createCell(9); HSSFCell signtime =
		 * row.createCell(10);
		 * contractnumber.setCellValue(importAndExport.getContractnumber());
		 * partners.setCellValue(importAndExport.getPartners());
		 * contractname.setCellValue(importAndExport.getContractname());
		 * clerk.setCellValue(importAndExport.getClerk());
		 * contractdetails.setCellValue(importAndExport.getContractdetails());
		 * totalamount.setCellValue(importAndExport.getTotalamount());
		 * auditamount.setCellValue(importAndExport.getAuditamount());
		 * received.setCellValue(importAndExport.getReceived());
		 * balance.setCellValue(importAndExport.getBalance());
		 * fundcontent.setCellValue(importAndExport.getFundcontent());
		 * signtime.setCellValue(importAndExport.getSigntime());
		 * 
		 * } } File file = new File(path); //System.out.println(file.getAbsolutePath());
		 * OutputStream os = new FileOutputStream(file);
		 * //System.out.println(Common.WRITE_DATA + path); book.write(os); os.close(); }
		 * 
		 * public void writeXlsx(List<ImportAndExport> list, String path) throws
		 * Exception { if (list == null) { return; } //XSSFWorkbook int countColumnNum =
		 * list.size(); XSSFWorkbook book = new XSSFWorkbook(); XSSFSheet sheet =
		 * book.createSheet("contractsheet"); // option at first row. XSSFRow firstRow =
		 * sheet.createRow(0); XSSFCell[] firstCells = new XSSFCell[countColumnNum];
		 * String[] options = { "合同编号", "客户公司名称", "合同名称", "业务�?", "合同备注", "合同金额",
		 * "审计金额", "收款金额", "未收金额", "款项内容","签订日期" }; for (int j = 0; j < options.length;
		 * j++) { firstCells[0] = firstRow.createCell(j); firstCells[0].setCellValue(new
		 * XSSFRichTextString(options[j])); } // for (int i = 0; i < countColumnNum;
		 * i++) { XSSFRow row = sheet.createRow(i + 1); ImportAndExport importAndExport
		 * = list.get(i); for (int column = 0; column < options.length; column++) {
		 * XSSFCell contractnumber = row.createCell(0); XSSFCell partners =
		 * row.createCell(1); XSSFCell contractname = row.createCell(2); XSSFCell clerk
		 * = row.createCell(3); XSSFCell contractdetails = row.createCell(4); XSSFCell
		 * totalamount = row.createCell(5); XSSFCell auditamount = row.createCell(6);
		 * XSSFCell received = row.createCell(7); XSSFCell balance = row.createCell(8);
		 * XSSFCell fundcontent = row.createCell(9); XSSFCell signtime =
		 * row.createCell(10);
		 * contractnumber.setCellValue(importAndExport.getContractnumber());
		 * partners.setCellValue(importAndExport.getPartners());
		 * contractname.setCellValue(importAndExport.getContractname());
		 * clerk.setCellValue(importAndExport.getClerk());
		 * contractdetails.setCellValue(importAndExport.getContractdetails());
		 * totalamount.setCellValue(importAndExport.getTotalamount());
		 * auditamount.setCellValue(importAndExport.getAuditamount());
		 * received.setCellValue(importAndExport.getReceived());
		 * balance.setCellValue(importAndExport.getBalance());
		 * fundcontent.setCellValue(importAndExport.getFundcontent());
		 * signtime.setCellValue(importAndExport.getSigntime()); } } File file = new
		 * File(path); OutputStream os = new FileOutputStream(file);
		 * //System.out.println(Common.WRITE_DATA + path); book.write(os); os.close(); }
		 * }
		 */