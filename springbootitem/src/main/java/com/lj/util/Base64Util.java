package com.lj.util;

import org.apache.commons.io.FileUtils;
import org.springframework.util.Base64Utils;

import java.io.File;
import java.util.UUID;

public class Base64Util {
    public static void main(String[] args) throws Exception {
        //JSONObject json = new JSONObject();
        String base64Data = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARQA…SJEmSJEmSJEmSffz00/8AFzWcmN/51qIAAAAASUVORK5CYII=";
        System.out.println("base64Data:"+base64Data);
        try{
            String dataPrix = "";
            String data = "";
            if(base64Data == null || "".equals(base64Data)){
                throw new Exception("上传失败，上传图片数据为�?");
            }else{
                String [] d = base64Data.split("base64,");
                if(d != null && d.length == 2){
                    dataPrix = d[0];
                    data = d[1];
                }else{
                    throw new Exception("上传失败，数据不合法");
                }
            }
            System.out.println("dataPrix:"+dataPrix);
            System.out.println("data:"+data);
            String suffix = "";
            if("data:image/jpeg;".equalsIgnoreCase(dataPrix)){//data:image/jpeg;base64,base64编码的jpeg图片数据
                suffix = ".jpg";
            } else if("data:image/x-icon;".equalsIgnoreCase(dataPrix)){//data:image/x-icon;base64,base64编码的icon图片数据
                suffix = ".ico";
            } else if("data:image/gif;".equalsIgnoreCase(dataPrix)){//data:image/gif;base64,base64编码的gif图片数据
                suffix = ".gif";
            } else if("data:image/png;".equalsIgnoreCase(dataPrix)){//data:image/png;base64,base64编码的png图片数据
                suffix = ".png";
            }else{
                throw new Exception("上传图片格式不合�?");
            }
            String tempFileName = UUID.randomUUID().toString() + suffix;
            System.out.println("tempFileName:"+tempFileName);
            //因为BASE64Decoder的jar问题，此处使用spring框架提供的工具包
            byte[] bs = Base64Utils.decodeFromString(data);
            try{
                //使用apache提供的工具类操作�?

                //System.out.println(request.getServletContext().getRealPath("/upload"));
                String path = "http://127.0.0.1:8080/matchproject/img/itemphoto";
                FileUtils.writeByteArrayToFile(new File(path, tempFileName), bs);
            }catch(Exception ee){
                throw new Exception("上传失败，写入文件失败，"+ee.getMessage());
            }

           // json.put("flag", true);
           // return json;
        }catch (Exception e) {
            //json.put("flag", true);
            //return json;
        }
    }
}

