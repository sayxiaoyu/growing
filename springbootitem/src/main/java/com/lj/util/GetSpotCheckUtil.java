package com.lj.util;

public class GetSpotCheckUtil {
	//获取抽检个数，简单随机采样计算公式
	public int getSamplingNumber(int recovery_number) {
		int samplingNumber=0;
			int	samplingNumber2 = 0;
		double f_e = 0;
		double f_checknumber = 0;
		double f_z=1.96;                                      //Z置信度95%对应值
		double f_p=0.95;            //置信度95%

		//e期望的误差界限,样品基数越大
		if (recovery_number >= 0 && recovery_number <= 100 )
		{
		f_e=0.1;
		}
		if (recovery_number > 100 && recovery_number <= 110)
		{  
		f_e=0.095;
		}
		if (recovery_number > 110 && recovery_number <= 123)
		{
		f_e=0.09;
		}
		if (recovery_number > 123 && recovery_number <= 138)
		{  
		f_e=0.085;
		}
		if (recovery_number > 138 && recovery_number<=156)
		{  
		f_e=0.08;
		}
		if (recovery_number > 156 && recovery_number <= 178)
		{  
		f_e=0.075;
		}
		if (recovery_number > 178 && recovery_number <= 204)
		{  
		f_e=0.07;
		}
		if (recovery_number > 204 && recovery_number <= 237)
		{  
		f_e=0.065;
		}
		if (recovery_number > 237 && recovery_number <= 277)
		{  
		f_e=0.06;
		}
		if (recovery_number > 277 && recovery_number <= 320)
		{  
		f_e=0.055;
		}
		if (recovery_number > 320 && recovery_number <= 400)
		{  
		f_e=0.05;
		}
		if (recovery_number > 400 && recovery_number <= 494)
		{  
		f_e=0.045;
		}
		if (recovery_number > 494 && recovery_number <= 625)
		{  
		f_e=0.04;
		}
		if (recovery_number > 625 && recovery_number <= 816)
		{  
		f_e=0.035;
		}
		if (recovery_number > 816 && recovery_number <= 1100)
		{  
		f_e=0.03;
		}
		if (recovery_number > 1100 && recovery_number <= 2500)
		{  
		f_e=0.02;
		}
		if (recovery_number > 2500)
		{  
		f_e=0.01;
		}
		f_checknumber = f_z*f_z*f_p*(1-f_p)/(f_e*f_e); //根据公式计算
		samplingNumber2 = (int)f_checknumber;
		samplingNumber
		=(int) (samplingNumber2*recovery_number/(samplingNumber2+recovery_number)); //调整初始样本用量
		return samplingNumber;
	}
	public static void main(String[] args) {
		GetSpotCheckUtil g = new GetSpotCheckUtil();
		System.out.println(g.getSamplingNumber(25));
	}
}
	

