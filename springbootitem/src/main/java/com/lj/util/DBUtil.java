package com.lj.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;

public class DBUtil {
	static BasicDataSource pool;
	static{//初始化，配置文件
		Properties pro = new Properties();
		try {
			pro.load(new FileInputStream(DBUtil.class.getResource("config.properties").getPath()));
			//String driver = pro.getProperty("oracleDriver");
			String url = pro.getProperty("mysqlUrl");
			String username = pro.getProperty("mysqlUsername");
			String password = pro.getProperty("mysqlPassword");
			int maxActive = Integer.parseInt(pro.getProperty("maxActive"));
			pool = new BasicDataSource();
			//设置连接池相关属性

			//Class.forName(driver);
			//pool.setDriverClassName(driver);
			pool.setDriverClassName("com.p6spy.engine.spy.P6SpyDriver");
			//设置连接参数
			pool.setUrl(url);
			pool.setUsername(username);
			pool.setPassword(password);
			//�?大同时连接数
			pool.setMaxActive(maxActive);
			//最长等待时间
			pool.setMaxWait(3000);
			//设置最小闲置数
			pool.setMinIdle(1);//默认最小闲置数
			//设置最大闲置数
			pool.setMaxIdle(5);//默认最大闲置数

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static Connection getConnection(){
		try {
			/*  从连接池中获取连接创建连接�?(必要�?)
			            会判断连接池�?大连接数
				如果连接数没有达到最大�?�，将创建连接返�?
			  	如果达到�?大�?�，再获取连接，将进入阻塞状�?
				如果在最大等待时间到达之前有闲置连接，将返回此连�?
				如果超过�?大等待时间，将不再等待，抛出异常
*/			return pool.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static void closeConnection(Connection conn){
		try {
			if(conn != null){
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		Connection connection1 = DBUtil.getConnection();
		Connection connection2 = DBUtil.getConnection();
		System.out.println(connection1);
		DBUtil.closeConnection(connection1);
		Connection connection3 = DBUtil.getConnection();
		System.out.println(connection1);
		System.out.println(connection2);
		System.out.println(connection3);
		/*DBUtil.closeConnection(connection);
		System.out.println(connection);*/
	}
}
