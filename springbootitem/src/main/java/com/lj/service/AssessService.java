package com.lj.service;

import java.util.Map;

public interface AssessService {
	
	Map<String,Object> getRegionKPI(String regionid,String sdate,String edate);
}
