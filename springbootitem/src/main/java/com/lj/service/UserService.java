package com.lj.service;

import java.util.List;

import com.lj.domain.UserInfo;

public interface UserService {
	public List<UserInfo> getUsersByName(String username);
}
