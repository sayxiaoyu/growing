package com.lj.service.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lj.domain.UserInfo;
import com.lj.mapper.UserMapper;
import com.lj.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
    private UserMapper userRepository;
	
	@Override
	public List<UserInfo> getUsersByName(String username) {
		return userRepository.getUsersByName(username);
	}

}
