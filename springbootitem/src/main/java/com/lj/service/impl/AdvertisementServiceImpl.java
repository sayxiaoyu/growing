package com.lj.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lj.domain.Advertisement;
import com.lj.mapper.AdvertisementMapper;
import com.lj.service.AdvertisementService;

import java.util.List;

@Service
public class AdvertisementServiceImpl implements AdvertisementService {

	@Autowired
	AdvertisementMapper advertisementMapper;

	@Override
	public List<Advertisement> getADByCollectorID(String collector_id) {
		
		return advertisementMapper.getADByCollectorID(collector_id);
		
	}

}
