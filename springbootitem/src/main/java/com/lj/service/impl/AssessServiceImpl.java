package com.lj.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lj.mapper.AssessMapper;
import com.lj.service.AssessService;

@Service
public class AssessServiceImpl implements AssessService {
	
	@Autowired
    private AssessMapper assessRepository;
	
	@Override
	public Map<String, Object> getRegionKPI(String regionid, String sdate, String edate) {
		return assessRepository.getRegionKPI(regionid, sdate, edate);
	}

}
