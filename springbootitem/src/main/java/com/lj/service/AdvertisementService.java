package com.lj.service;

import java.util.List;

import com.lj.domain.Advertisement;

public interface AdvertisementService {
	List<Advertisement> getADByCollectorID(String collector_id);
}
